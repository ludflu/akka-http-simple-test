
organization := "com.snavely"

name := "akka-http-test"

version := "0.1"

scalaVersion := "2.12.8"

val AkkaHttpVersion   = "10.1.11"
val circeVersion = "0.13.0"

enablePlugins(DockerPlugin)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "de.knutwalker" %% "akka-stream-circe" % "3.5.0",
  "de.knutwalker" %% "akka-http-circe" % "3.5.0",
  "com.typesafe.akka" %% "akka-stream" % "2.5.31"
)

dockerfile in docker := {
  // The assembly task generates a fat JAR file
  val artifact: File = assembly.value
  val artifactTargetPath = s"/app/${artifact.name}"

  new Dockerfile {
    from("openjdk:8-jre")
    add(artifact, artifactTargetPath)
    entryPoint("java", "-jar", artifactTargetPath)
  }
}

