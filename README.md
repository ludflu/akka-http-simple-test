# Minimal Akka-http web service

## Shows example code for:

- serving simple/static responses
- streaming responses
- streaming requests
- websocket flows
- route composition


## to run:

`sbt run`

then connect via browser to: 

http://localhost:8080/test/hello

http://localhost:8080/test/stream

ws://localhost:8080/test/chat

## to build a docker image

`sbt docker`

(image available at https://cloud.docker.com/repository/docker/ludflu/akka-http-test )

`docker run ludflu/akka-http-test:latest`

## to run in kubernetes:

`kubectl apply -f ./akka-http.yaml`

Then expose it as a service:

`kubectl expose deployment akka-http --type=LoadBalancer --name=akka-http-service`

You should now be able to point your browser and see output from localhost:8080 at the urls above.

