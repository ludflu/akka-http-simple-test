import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.{Materializer, OverflowStrategy, QueueOfferResult}
import akka.stream.scaladsl.{Framing, Keep, Sink, Source}
import akka.util.ByteString

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.{Failure, Success}

// https://download.bls.gov/pub/time.series/cu/cu.data.11.USFoodBeverage

class WSClient(host:String = "download.bls.gov", reuqestQueueSize:Int=2)(implicit ec:ExecutionContext, as:ActorSystem, m:Materializer) {

  // This idea came initially from this blog post:
  // http://kazuhiro.github.io/scala/akka/akka-http/akka-streams/2016/01/31/connection-pooling-with-akka-http-and-source-queue.html
  val poolClientFlow = Http().cachedHostConnectionPoolHttps[Promise[HttpResponse]](host=host)
  val queue =
    Source.queue[(HttpRequest, Promise[HttpResponse])](reuqestQueueSize, OverflowStrategy.dropHead)
      .via(poolClientFlow)
      .toMat(Sink.foreach({
        case ((Success(resp), p)) => p.success(resp)
        case ((Failure(e), p))    => p.failure(e)
      }))(Keep.left)
      .run()

  def queueRequest(request: HttpRequest): Future[HttpResponse] = {
    val responsePromise = Promise[HttpResponse]()
    queue.offer(request -> responsePromise).flatMap {
      case QueueOfferResult.Enqueued    => responsePromise.future
      case QueueOfferResult.Dropped     => responsePromise.future  //we dropped an element, but the new item can proceed
      case QueueOfferResult.Failure(ex) => Future.failed(ex)
      case QueueOfferResult.QueueClosed => Future.failed(new RuntimeException("Queue was closed (pool shut down) while running the request. Try again later."))
    }
  }

  def getStats: Future[Source[String, Any]] = {
    val delimiter = Framing.delimiter(ByteString("\n"), maximumFrameLength=1024)
    for {
      response <- queueRequest(HttpRequest(uri="/pub/time.series/cu/cu.data.11.USFoodBeverage"))
    } yield response.entity.dataBytes.via(delimiter).map(_.utf8String)
  }

}
