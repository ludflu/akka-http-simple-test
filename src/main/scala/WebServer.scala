
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.common.{EntityStreamingSupport, JsonEntityStreamingSupport}
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.http.scaladsl.server.{Directives, Route}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Source}
import de.knutwalker.akka.http.support.CirceHttpSupport._

object WebServer extends App with Directives {

  implicit lazy val system: ActorSystem = ActorSystem("test-akka-http")
  implicit val executionContext = system.dispatcher
  implicit lazy val materializer: ActorMaterializer = ActorMaterializer()
  implicit val jsonStreamingSupport: JsonEntityStreamingSupport = EntityStreamingSupport.json()

  val wsclient= new WSClient()

  //serving static text
  def hello: Route = path("hello") {
      complete("hi")
  }

  //streamed response from a webservice request used to stream the response for this request
  def stats : Route = path("stats") {
    complete {
      wsclient.getStats
    }
  }

  //streamed source
  def stream : Route = path("stream") {
    complete{
      Source((1 to 50).toList)
    }
  }

  //using a flow to serve a websocket connection
  def reverseChat : Route = path("chat") {
    handleWebSocketMessages {
      Flow[Message].map{ msg =>
          TextMessage(msg.asTextMessage.getStrictText.reverse) }
    }
  }


  val routes = pathPrefix("test") {
    concat(stream,hello,reverseChat, stats)
  }

  Http().bindAndHandle(routes, "0.0.0.0", 8080)

  println(s"Server online !")

}